# README

This is an implementation of a todo list in plain HTML, CSS and javascript.
It uses the `localStorage` to keep the user data and his todo lists.

## Features

The user can:

- sign up
- login/logout
- create a todo list
- create tasks inside those todo lists.

## Installation

No installation is needed, just open it in the browser.


## Contributors

- [[sespinoz]](https://github.com/sespinoza-dev) Samuel Espinoza - author.
