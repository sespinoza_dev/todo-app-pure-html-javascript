
window.addEventListener("load", onLoad)

function mapTasks (htmlObj) {
  let array = []
  for (let i = 0; i < htmlObj.length; i++) {
    array[i] = {
      text: htmlObj[i].innerHTML,
      status: htmlObj[i].classList.contains('done') ? 'done' : 'undone'
    }
  }
  return array
}

/**
 * transform a string to the form this-is-a-slug
 * @params  {String} email the user's email
 * @returns {Strging}      the string version of the email
 */
function slugify(email) {
  return email.toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^a-z0-9 -]/g, '')
    .replace(/-+/g, '-')
    .replace(/^\s+|\s+$/g, '')
}

/**
 * get user data from localStorage
 * @params  {String} email user's email
 * @returns {Object} user data
 */
function getUser(email) {
  const slug = slugify(email)
  const tempStorage = localStorage.getItem(slug)
  const user = JSON.parse(tempStorage)
  return user
}

/**
 * display error span
 * @params  {String}    fieldName either name, surname, email, password
 * @params  {String}    msg       error message to display
 * @returns {undefined}
 */
function showError(fieldName, msg) {
  const errorNode = document.getElementById(`${fieldName}-error`)
  errorNode.classList.add('active')
  errorNode.classList.remove('inactive')
  errorNode.classList.add('form-error')
  errorNode.innerHTML = msg
}

/**
 * signin and loginvalidations
 * @params  {String} type either signin or login
 * @returns {Object} user data
 */
function formValidations (type, _email) {
  let email = _email
  if (type != 'update') {
    email = document.getElementById('email').value
  }
  const password = document.getElementById('password').value
  let thereIsAnError = false


  let name, surname
  if (type == 'signin' || type == 'update') {
    name = document.getElementById('name').value
    surname = document.getElementById('surname').value
    if (name == "") {
      showError('name', 'name is required')
      thereIsAnError = true
    }
    if (surname == "") {
      showError('surname', 'surname is required')
      thereIsAnError = true
    }
  }

  if (type != 'update') {
    if (email == "") {
      showError('email', 'email is required')
      thereIsAnError = true
    }
  }

  if (password == "") {
    showError('password', 'password is required')
    thereIsAnError = true
  }

  const userData = getUser(email)
  if (userData) {
    if (type === 'signin') {
      showError('email', 'email is taken')
      thereIsAnError = true
    } else if (userData.user_configuration.password != password && type == 'login') {
      showError('password', 'incorrect password')
      thereIsAnError = true
    }
  } else if (type == 'login') { // user does not exist
    showError('email', 'This user does not exist, sign in first')
    thereIsAnError = true
  }

  if (thereIsAnError) {
    throw Error('some fields are empty!')
  } else {
    if (type !== 'login') {
      return ({
        user_configuration: {
          name,
          surname,
          email,
          password
        }
      })
    } else {
      return userData
    }
  }
}

/**
 * appends a new field with label, input and modular error handling.
 * @params {String} type either text or password.
 * @params {String} name field name.
 * @params {Object} form the form object.
 */
function appendFormField(type, name, _default, form) {
  // label
  const label = document.createElement('label')
  label.innerHTML= name.replace(/^\w/, c => c.toUpperCase())

  // error span
  const fieldFormError = document.createElement('span')
  fieldFormError.classList.add('inactive')
  fieldFormError.id = `${name}-error`

  // text input
  const inputField = document.createElement('input')
  inputField.type=type
  inputField.id=name
  inputField.value=_default
  inputField.addEventListener('keyup', () => {
    fieldFormError.classList.add('inactive')
    fieldFormError.classList.remove('active')
  })

  form.appendChild(label)
  form.appendChild(inputField)
  form.appendChild(fieldFormError)
}

/**
 * creates a signin form or a login form depending on the type.
 * @params  {String}   type     either signin or login.
 * @params  {function} callBack it triggers when the form is submitted.
 * @returns {Object}   form     return the html form element.
 */
function createForm (type, _default, callBack) {
  const form = document.createElement('form')
  form.classList.add('form')

  // fields
  if (type == 'signin' || type == 'update') {
    appendFormField('text', 'name', _default.name, form)
    appendFormField('text', 'surname', _default.surname, form)
  }
  if (type != 'update') {
    appendFormField('text', 'email', '', form)
  }
  appendFormField('password', 'password', '', form)

  // submit
  const submitSignIn = document.createElement('button')
  submitSignIn.type = 'submit'
  submitSignIn.innerHTML = type == 'login' ?
    'login' :
    type == 'signin' ?
      'signin' :
      'update'

  // handle submit form
  submitSignIn.addEventListener("click", callBack)
  form.appendChild(submitSignIn)

  return form
}

function addTask (newTaskText, status, listElement, timestamp) {
  console.log('new task')
  const newTask = document.createElement('p')
  newTask.classList.add(`task-${timestamp}`, status)
  newTask.innerHTML = newTaskText
  newTask.addEventListener('click', e => {
    return newTask.classList.toggle('done')
  })
  listElement.insertBefore(newTask, listElement.children[2])
}

function displayTasks(listElement, list) {
  const taskForm = document.createElement('div')
  taskForm.classList.add('task-form')
  const taskInput = document.createElement('input')
  taskInput.type = 'text'
  const taskButton = document.createElement('button')
  taskButton.innerHTML = '+'
  taskForm.appendChild(taskInput)
  let newTaskText = ''
  taskForm.addEventListener('keyup', e => {
    newTaskText = e.target.value
  })
  taskForm.appendChild(taskButton)
  taskButton.addEventListener('click', e => {
    return addTask(newTaskText, 'undone', listElement, list.timestamp)
  })
  listElement.appendChild(taskForm)

  if (list && list.tasks && list.tasks.length != 0) {
    list.tasks.forEach(item => addTask(item.text, item.status, listElement, list.timestamp))
  }
}

function getTaksElements (timestamp) {
  const tasksElements = document.getElementsByClassName(`task-${timestamp}`)
  const taskArray = mapTasks(tasksElements)
  return taskArray
}

function saveList(newTitle, list, email) {
  if (newTitle == '') {
    alert('empty title!')
    return
  }
  let todoList
  const userData = getUser(email)
  const isNewList = list.timestamp == null
  const tasksUpdated = getTaksElements(list.timestamp)
  if (isNewList) {
    const found = userData.todo_list.find(item => item.name == newTitle)
    if (found) {
      alert('There is already a list with that name.')
      return
    }
    list.timestamp = Date.now()
    todoList = [...userData.todo_list, { name: newTitle, timestamp: Date.now(), tasks: tasksUpdated }]
  } else {
    const rest = userData.todo_list.filter(item => item.timestamp != list.timestamp)
    const found = rest.find(item => item.name == list.name)
    if (found) {
      alert('There is already a list with that name.')
      return
    }
    todoList = [...rest, { name: newTitle, timestamp: list.timestamp , tasks: tasksUpdated }]
  }
  userData.todo_list = todoList
  const slug = slugify(email)
  localStorage.setItem(slug, JSON.stringify(userData));
  console.log('listElement saved')
  console.log(todoList)
}

function deleteList(originalName, email, dashboardContainer, listElement) {
  dashboardContainer.removeChild(listElement)
  const userData = getUser(email)
  const todoList = userData.todo_list.filter(listElement => listElement.name != originalName)
  userData.todo_list = todoList
  const slug = slugify(userData.user_configuration.email)
  localStorage.setItem(slug, JSON.stringify(userData));
  console.log('listElement saved')
  console.log(todoList)
}

function appendNewList(dashboardContainer, list, email) {
  console.log(`list ${list.name} t:${list.timestamp}`)
  const originalName = list && list.name || ''
  const listElement = document.createElement('div')
  listElement.classList.add('list')
  const buttonWrapper = document.createElement('div')
  const inputListName = document.createElement('input')

  // action buttons for listElement
  const saveListButton = document.createElement('button')
  const deleteListButton = document.createElement('button')
  deleteListButton.classList.add('delete-button')
  saveListButton.classList.add('save-button')
  inputListName.type = 'text'
  deleteListButton.innerHTML = 'drop list'
  saveListButton.innerHTML = 'save list'

  let newTitle = list && list.name || ''
  inputListName.value = newTitle
  inputListName.addEventListener('keyup', e => {
    const inputData = e.target.value
    newTitle = inputData
  })

  saveListButton.addEventListener('click', e => {
    return saveList(newTitle, list, email)
  })

  deleteListButton.addEventListener('click', e => {
    return deleteList(originalName, email, dashboardContainer, listElement)
  })

  dashboardContainer.appendChild(listElement)
  listElement.appendChild(inputListName)

  displayTasks(listElement, list)
  buttonWrapper.classList.add('bottom')
  buttonWrapper.appendChild(saveListButton)
  buttonWrapper.appendChild(deleteListButton)
  listElement.appendChild(buttonWrapper)
}

function displayDashboard(slug, userData) {
  const todoContainer = document.getElementById('todo-container')
  const todoListTopWrapper = document.createElement('div')
  todoListTopWrapper.classList.add('todo-list-top-wrapper')
  const title = document.createElement('h1')
  title.innerHTML = 'Your todo lists'
  todoListTopWrapper.appendChild(title)

  const dashboardWrapper = document.createElement('div')
  dashboardWrapper.classList.add('dashboard-wrapper')

  const dashboardContainer = document.createElement('div')
  dashboardContainer.classList.add('dashboard-container')

  const lists = userData.todo_list
  if (lists && lists != []) {
    lists.forEach(list => appendNewList(dashboardContainer, list, userData.user_configuration.email))
  }

  const button = document.createElement('button')
  button.classList.add('new-list-button')
  button.innerHTML = 'New list'
  button.addEventListener('click', () => appendNewList(dashboardContainer, { name: null, timestamp: null }, userData.user_configuration.email))

  todoListTopWrapper.appendChild(button)
  todoContainer.appendChild(todoListTopWrapper)
  dashboardWrapper.appendChild(dashboardContainer)
  todoContainer.appendChild(dashboardWrapper)
}

function handleLogout(slug, email) {
  const user = getUser(email)
  user.isLoggedIn = false
  localStorage.setItem(slug, JSON.stringify(user));
  const dashboard = document.getElementsByClassName('dashboard-wrapper')[0]
  dashboard.remove()
  const todoListTopWrapper = document.getElementsByClassName('todo-list-top-wrapper')[0]
  todoListTopWrapper.remove()
  const headerOptions = document.getElementsByClassName('header-options-wrapper')[0]
  headerOptions.remove()
  const buttonsWrapper = document.getElementsByClassName('buttons-wrapper')[0]
  buttonsWrapper.classList.add('active')
  buttonsWrapper.classList.remove('inactive')
}

function handleSettings(slug, email) {
  const dashboard = document.getElementsByClassName('dashboard-wrapper')[0]
  dashboard.remove()
  const todoListTopWrapper = document.getElementsByClassName('todo-list-top-wrapper')[0]
  todoListTopWrapper.remove()
  const buttonsWrapper = document.getElementsByClassName('buttons-wrapper')[0]
  buttonsWrapper.classList.add('inactive')
  buttonsWrapper.classList.remove('active')
  const headerOptions = document.getElementsByClassName('header-options-wrapper')[0]
  headerOptions.remove()

  const todoContainer = document.getElementById('todo-container')
  const formContainer = document.createElement('div')
  formContainer.classList.add('authentication-container')

  // form
  const userData = getUser(email)
  const _default = {
    name: userData.user_configuration.name,
    surname: userData.user_configuration.surname
  }
  const form = createForm('update', _default, handleSubmit('update', email))
  formContainer.appendChild(form)
  todoContainer.appendChild(formContainer)
}

function headerOptions(slug, email) {
  const header = document.getElementsByClassName('header')[0]

  const optionsWrapper = document.createElement('div')
  optionsWrapper.classList.add('header-options-wrapper')

  const logoutButton = document.createElement('button')
  logoutButton.classList.add('header-option')
  logoutButton.innerHTML = 'log out'
  logoutButton.addEventListener('click', () => {
    console.log('log out')
    handleLogout(slug, email)
  })

  const configurations = document.createElement('p')
  configurations.classList.add('header-option')
  configurations.innerHTML = 'account settings'
  configurations.addEventListener('click', () => {
    console.log('account settings')
    handleSettings(slug, email)
  })

  optionsWrapper.appendChild(configurations)
  optionsWrapper.appendChild(logoutButton)
  header.appendChild(optionsWrapper)
}

/**
 * handle both signin and login form submits.
 * handleSubmit :: string -> object -> undefined
 */
const handleSubmit = (type, _email) => e => {
  e.preventDefault()
  try {
    if (type === 'signin') {
      console.log('signin')
      const userData = formValidations(type)
      const { name, surname, email, password } = userData.user_configuration
      const slug = slugify(email)
      const payload = {
        user_configuration: {
          isLoggedIn: true,
          slug,
          name,
          surname,
          email,
          password
        },
        todo_list: []
      }
      localStorage.setItem(slug, JSON.stringify(payload));
      console.log(userData)
      const form = document.getElementsByClassName('authentication-wrapper')
      while(form[0]) {
        form[0].parentNode.removeChild(form[0]);
      }
      headerOptions(slug, email)
      displayDashboard(slug, userData)
    } else if (type === 'login') {
       // login
      console.log('login')
      const userData = formValidations(type)
      const { name, surname, email, password } = userData.user_configuration
      const slug = slugify(email)
      const form = document.getElementsByClassName('authentication-wrapper')
      while(form[0]) {
        form[0].parentNode.removeChild(form[0]);
      }

      headerOptions(slug, email)
      displayDashboard(slug, userData)
    } else {
      // update
      console.log('update update')
      const userData = formValidations(type, _email)
      const { name, surname, email, password } = userData.user_configuration
      const slug = slugify(email)
      const userDataBefore = getUser(email)
      userData.todo_list = userDataBefore.todo_list

      console.log('userData to be saved:')
      console.log(userData)
      localStorage.setItem(slug, JSON.stringify(userData));
      const form = document.getElementsByClassName('authentication-container')
      while(form[0]) {
        form[0].parentNode.removeChild(form[0]);
      }

      headerOptions(slug, email)
      displayDashboard(slug, userData)
    }
  } catch (error) {
    console.log('There are some errors in the form')
    console.error(error)
  }
}

function signIn() {
  console.log('signin')
  const buttonsWrapper = document.getElementsByClassName('buttons-wrapper')
  buttonsWrapper[0].classList.add('inactive')

  // wrappers and containers
  const container = document.getElementById('todo-container')
  const formWrapper = document.createElement('div')
  formWrapper.classList.add('authentication-wrapper')
  const formContainer = document.createElement('div')
  formContainer.classList.add('authentication-container')

  // form
  const _default = {
    name: '',
    surname: ''
  }
  const form = createForm('signin', _default, handleSubmit('signin'))
  formContainer.appendChild(form)
  formWrapper.appendChild(formContainer)
  container.appendChild(formWrapper)
}

function login() {
  console.log('login')
  const buttonsWrapper = document.getElementsByClassName('buttons-wrapper')
  buttonsWrapper[0].classList.add('inactive')

  // wrappers and containers
  const container = document.getElementById('todo-container')
  const formWrapper = document.createElement('div')
  formWrapper.classList.add('authentication-wrapper')
  const formContainer = document.createElement('div')
  formContainer.classList.add('authentication-container')

  const _default = {
    name: '',
    surname: ''
  }
  const form = createForm('login', _default, handleSubmit('login'))
  formContainer.appendChild(form)
  formWrapper.appendChild(formContainer)
  container.appendChild(formWrapper)
}

function onLoad() {
  console.log('loaded')
  const header = `<div class="header"></div>`
  const wrapper =`<div class="content-wrapper">
        <div class="about-panel">
          <h1>Todo List</h1>
          <h2>We help you boost your productivity</h2>
          <p>
            This is a sample app that uses plain html,
            css and javascript for creating todo lists.
          </p>
          <p>
            Here you can:
            <ul>
              <li>Create a personal account</li>
              <li>Create todo lists</li>
              <li>Create tasks</li>
              <li>Mark tasks as completed</li>
            </ul>
          </p>
        </div>
        <div id="todo-container">
          <div class="buttons-wrapper">
            <div class="buttons">
              <button class="signin" onclick="signIn()">Sign in</button>
              <button class="login" onclick="login()">login</button>
            </div>
          </div>
        </div>
      </div>`
  const footer = `<footer>
      <div class="footer-wrapper">
        <p>Copyright &copy; 2020 S.Espinoza. </p>
        <p><a href="mailto:espinoza.jimenez@gmail.com">espinoza.jimenez@gmail.com</a></p>
      </div>
    </footer>`
  document.body.innerHTML = `${header}\n${wrapper}\n${footer}`
}

